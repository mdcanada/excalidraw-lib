if (process.env.REACT_APP_ENV === "production") {
  module.exports = require("./dist/excalidraw.production.min.js");
} else if (process.env.REACT_APP_ENV === "staging") {
  module.exports = require("./dist/excalidraw.staging.min.js");
} else {
  module.exports = require("./dist/excalidraw.development.js");
}
